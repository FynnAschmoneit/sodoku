// https://www.youtube.com/watch?v=G_UYXzGuqvM

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;


int counter = 0;

void printGrid(vector<vector<int>>& grid){
    cout << endl;
    for(int y=0; y!=9; ++y){
	for( int x=0; x!=9; ++x){
	    cout << grid[y][x] << "  ";
	    if((x-2)%3==0 && x!=8){
		cout << "|";
	    }
	}
	cout << endl;
	if((y-2)%3==0 && y!=8){
	    cout << "____________________________" << endl;;
	}
    }
    cout << endl;
}


bool possible(int y, int x, int n, vector<vector<int>>& g){
    vector<int> line = g[y];
    vector<int> column = {
	g[0][x], g[1][x], g[2][x], g[3][x], g[4][x], g[5][x], g[6][x], g[7][x], g[8][x]
    };
    vector<int> square = {
	g[3*(y/3)][3*(x/3)], g[3*(y/3)][3*( x/3)+1], g[3*(y/3)][3*(x/3)+2],
	g[3*(y/3)+1][3*(x/3)], g[3*(y/3)+1][3*( x/3)+1], g[3*(y/3)+1][3*(x/3)+2],
	g[3*(y/3)+2][3*(x/3)], g[3*(y/3)+2][3*( x/3)+1], g[3*(y/3)+2][3*(x/3)+2]
    };

    if( find(line.begin(), line.end(), n) != line.end() ) return false;
    if( find(column.begin(), column.end(), n) != column.end() ) return false;
    if( find(square.begin(), square.end(), n) != square.end() ) return false;
    
    return true;
}

bool findEmptyPosition(int& y, int& x, vector<vector<int>>& g){
    for(int i=0; i!=9; ++i){
	for(int j=0; j!=9; ++j){
	    if(g[i][j]==0){
		y = i;
		x = j;
		return true;
	    }
	}
    }
    return false;
}

bool solve(vector<vector<int>>& g){

    int y,x;

    if(!findEmptyPosition(y,x,g)){
	return true;
    }else{
	for(int n=1; n<=9; ++n){
	    if(possible(y,x,n,g)){
		g[y][x] = n;
		if( solve(g) ){
		    return true;
		}
		g[y][x] = 0;
	    }
	}
	return false;
    }
}


int main(){

    vector<vector<int>> grid{
	{0, 9, 0, 0, 1, 0, 0, 4, 0}, 
	{0, 0, 0, 9, 0, 4, 0, 0, 0}, 
	{8, 0, 4, 0, 0, 0, 7, 0, 9}, 
	{5, 0, 0, 1, 8, 3, 0, 0, 6}, 
	{1, 0, 0, 0, 7, 0, 0, 0, 8}, 
	{3, 0, 0, 6, 9, 2, 0, 0, 1}, 
	{2, 0, 6, 0, 0, 0, 9, 0, 4}, 
	{0, 0, 0, 5, 0, 9, 0, 0, 0}, 
	{0, 1, 0, 0, 4, 0, 0, 6, 0}    };
    
    printGrid(grid);
    
    bool b = solve(grid);
    printGrid(grid);
    
    return 1;
}
